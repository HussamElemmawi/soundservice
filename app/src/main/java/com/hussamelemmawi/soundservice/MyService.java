package com.hussamelemmawi.soundservice;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

/**
 * An {@link Service} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class MyService extends Service {
    // Service can perform, e.g. ACTION_FETCH_NEW_ITEMS
    static MediaPlayer sMediaPlayer;
    static int sPosition = 0;

    public static final String ACTION_PLAY = "com.hussamelemmawi.soundservice.action.PLAY";
    public static final String ACTION_PAUSE = "com.hussamelemmawi.soundservice.action.PAUSE";

    private volatile HandlerThread mHandlerThread;
    private ServiceHandler mServiceHandler;
    private MyRunnable playActionCodeBlock;

    @Override
    public void onCreate() {
        super.onCreate();

        mHandlerThread = new HandlerThread("MyService.HanlderThread");
        mHandlerThread.start();

        mServiceHandler = new ServiceHandler(mHandlerThread.getLooper());

        setupServiceNotification();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            final String action = intent.getAction();
            final String param1 = "media" /*intent.getStringExtra(EXTRA_PARAM1)*/;
            final String param2 = "media" /*intent.getStringExtra(EXTRA_PARAM2)*/;

            if (ACTION_PLAY.equals(action)) {
                if (mediaPlayerIsEmpty()) {
                    playActionCodeBlock = new MyRunnable(param1, param2);
                    mServiceHandler.post(playActionCodeBlock);
                } else if (!sMediaPlayer.isPlaying()) {
                    sMediaPlayer.start();
                }
            } else if (ACTION_PAUSE.equals(action)) {
                if (sMediaPlayer.isPlaying())
                    sMediaPlayer.pause();
            }
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandlerThread.quit();
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see Service
     */
    public void setupServiceNotification() {

        final PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent playIntent = new Intent(this, MyService.class);
        playIntent.setAction(ACTION_PLAY);

        final PendingIntent playIntentPendingIntent = PendingIntent.getService(
                this, 1, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent pauseIntent = new Intent(this, MyService.class);
        pauseIntent.setAction(ACTION_PAUSE);

        final PendingIntent pauseIntentPendingIntent = PendingIntent.getService(
                this, 1, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setOngoing(true)
                .setContentTitle(this.getString(R.string.app_name))
                .setContentIntent(pi)
                .setVisibility(android.support.v7.app.NotificationCompat.VISIBILITY_PUBLIC)
                .addAction(R.drawable.ic_play_arrow_black_24dp, this.getString(R.string.action_play),
                        playIntentPendingIntent)
                .addAction(R.drawable.ic_pause_black_24dp, this.getString(R.string.action_pause),
                        pauseIntentPendingIntent)

                .setShowWhen(true)
                .setWhen(1) // older platforms seem to ignore setShowWhen(false)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher));

        startForeground(1234, builder.build());
    }

    /**
     * Handle update the MediaPlayer to the current seeking position
     */
    public static boolean mediaPlayerIsEmpty() {
        return sMediaPlayer == null;
    }

    /**
     * Handle update the MediaPlayer to the current seeking position
     */
    public static void seekTo(int num) {
        sMediaPlayer.seekTo(num * 1000);
    }


    /**
     * Handle action Play in the provided background thread with the provided
     * parameters.
     */
    private class MyRunnable implements Runnable {

        private String mParam1, mParam2;

        MyRunnable(String param1, String param2) {
            mParam1 = param1;
            mParam2 = param2;
        }

        @Override
        public void run() {
            if (!mParam1.isEmpty()) {
                if (sPosition == 0) {
                    sMediaPlayer = MediaPlayer.create(MyService.this,
                            R.raw.quraan_elmozamel_elmenshawy);
                }
                sMediaPlayer.seekTo(sPosition);
                sMediaPlayer.start();
            } else {
                throw new UnsupportedOperationException("Cannot play empty sound");
            }
        }
    }

    private final class ServiceHandler extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }
    }
}
