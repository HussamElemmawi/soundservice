package com.hussamelemmawi.soundservice;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button mBtnPlay, mBtnPause;
    SeekBar mSeekBar;
    TextView mCurrentTimeTextView;
    Button mBtnNext, mBtnPrevious;
    private int mCurrentProgress = 0;
    private Handler mHandler;
    private Runnable mSeekBarUpdateCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeViews();
        initializeViewsActions();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mSeekBarUpdateCode);
    }


    private void initializeViews() {
        mBtnPlay = (Button) findViewById(R.id.btn_play);
        mBtnPause = (Button) findViewById(R.id.btn_pause);
        mSeekBar = (SeekBar) findViewById(R.id.seek_bar);
        mCurrentTimeTextView = (TextView) findViewById(R.id.text_time);

        mBtnNext = (Button) findViewById(R.id.button_next);
        mBtnPrevious = (Button) findViewById(R.id.button_previous);

        mHandler = new Handler();

        mSeekBarUpdateCode = new Runnable() {
            @Override
            public void run() {
                mHandler.postDelayed(this, 1000);
                if (!MyService.mediaPlayerIsEmpty()) {
                    mCurrentProgress = MyService.sMediaPlayer.getCurrentPosition() / 1000;
                    mSeekBar.setProgress(mCurrentProgress);
                    mCurrentTimeTextView.setText(Integer.toString(mCurrentProgress));
                }
            }
        };

        MainActivity.this.runOnUiThread(mSeekBarUpdateCode);
    }

    private void initializeViewsActions() {
        mBtnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyService.mediaPlayerIsEmpty()) {
                    Intent intent = new Intent(MainActivity.this, MyService.class);
                    intent.setAction(MyService.ACTION_PLAY);
                    startService(intent);
                } else {
                    if (!MyService.sMediaPlayer.isPlaying()) {
                        MyService.sMediaPlayer.start();
                    }
                }
            }
        });

        mBtnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MyService.mediaPlayerIsEmpty()) {
                    Toast.makeText(MainActivity.this, "Nothing to Pause", Toast.LENGTH_LONG).show();
                } else {
                    if (MyService.sMediaPlayer.isPlaying()) {
                        Intent intent = new Intent(MainActivity.this, MyService.class);
                        intent.setAction(MyService.ACTION_PAUSE);
                        startService(intent);
                    }
                }
            }
        });

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!MyService.mediaPlayerIsEmpty()) {
                    mSeekBar.setMax(MyService.sMediaPlayer.getDuration() / 1000);
                    mCurrentProgress = MyService.sMediaPlayer.getCurrentPosition() / 1000;
                    Log.d("debug", "mCurrentProgress " + mCurrentProgress);
                } else {
                    mSeekBar.setMax(0);
                    mCurrentProgress = 0;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mCurrentProgress = seekBar.getProgress();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mCurrentProgress = seekBar.getProgress();
                MyService.seekTo(mCurrentProgress);
                if (!MyService.sMediaPlayer.isPlaying()) {
                    MyService.sMediaPlayer.start();
                }
            }
        });

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyService.sMediaPlayer.release();
                MyService.sMediaPlayer = MediaPlayer.create(MainActivity.this,
                        Uri.parse("android.resource://" +
                                getPackageName() + "/" + R.raw.quraan_elmodether_elmenshawy));
                MyService.sMediaPlayer.start();
            }
        });

        mBtnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyService.sMediaPlayer.release();
                MyService.sMediaPlayer = MediaPlayer.create(MainActivity.this,
                        Uri.parse("android.resource://" +
                                getPackageName() + "/" + R.raw.quraan_elmozamel_elmenshawy));
                MyService.sMediaPlayer.start();
            }
        });
    }
}
